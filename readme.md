# How to Start a New Project With This Repo

1.  ```
    git remote add origin git@gitlab.com:Blankon349/template.git
    ```
    this will make this repo as a remote upstream for the corresponding folder
2.  ```
    git fetch origin
    ```
    fetching from template repo to this folder
3.  ```
    git merge origin/master master --allow-unrelated-histories -X theirs
    ```
    merge from template to main repo
4.  ```
    git remote set-url origin [the new upstream repo]
    ```
    to switch the upstream repo to the main one
5.  ```
    yarn
    ```

