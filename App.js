/* eslint-disable global-require */
import React from 'react';
import { Root } from 'native-base';
import { Font, AppLoading } from 'expo';
import {
  createSwitchNavigator,
  createAppContainer,
} from 'react-navigation';
import Login from './src/login/login';
import AuthCheck from './src/login/authCheck';
import Main from './src/main/main';
import About from './src/about/about';
import Report from './src/report/report';

const LoginNav = createSwitchNavigator(
  {
    Login,
    AuthCheck,
  }, {
    initialRouteName: 'Login',
  },
);

const AppNav = createAppContainer(LoginNav);
export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isLoaded: false };
  }

  async componentDidMount() {
    this.setState({ isLoaded: false });
    await Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
    });
    this.setState({ isLoaded: true });
  }

  render() {
    const { isLoaded } = this.state;
    if (!isLoaded) { return (<AppLoading />); }
    return (
      <Root>
        <Report />
      </Root>
    );
  }
}
