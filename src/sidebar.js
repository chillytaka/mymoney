import React, { Component } from 'react';
import {
  Image, View, AsyncStorage, TouchableOpacity,
} from 'react-native';
import {
  Content,
  Text,
  List,
  ListItem,
  Icon,
  Container,
  Left,
  Accordion,
} from 'native-base';
import styles from './common/style';

const drawerCover = require('../assets/logo.png');

const datas = [
  {
    name: 'Overview',
    icon: 'paper',
    route: 'overview',
  },
  {
    name: 'Add Report',
    icon: 'add',
    route: 'Report',
  },
  {
    name: 'Admin',
    icon: 'person',
    route: 'Report',
  },
];
const menu = [
  {
    title: 'Ticketing',
    content: [
      {
        name: 'List Report',
        icon: 'document',
        route: 'listNav',
        key: 0,
      },
    ],
  },
  {
    title: 'Task Description',
    content: [
      {
        name: 'List Report',
        icon: 'document',
        route: 'listNav',
        key: 0,
      },
    ],
  },
  {
    title: 'List Report',
    content: [
      {
        name: 'List Report',
        icon: 'document',
        route: 'listNav',
        key: 0,
      },
    ],
  },
];

class SideBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: 'Unknown Profile',
      tipe: 'unregistered',
    };
  }

  componentDidMount() {
    this.getUser();
    this.getItem();
  }

  // add menu and add it to its respective accordion
  getItem = () => {
    fetch('https://kemarilah.net/Dehael/listAudit.php')
      .then(response => response.json())
      .then((responseJson) => {
        for (let i = 0; i < responseJson.length; i += 1) {
          menu[0].content.push({
            name: responseJson[i].AUDITTYPE,
            icon: 'alert',
            route: 'Buffer',
            key: i + 1,
          });
          menu[1].content.push({
            name: responseJson[i].AUDITTYPE,
            icon: 'alert',
            route: 'Buffer',
            key: i + responseJson.length + 1,
          });
          menu[2].content.push({
            name: responseJson[i].AUDITTYPE,
            icon: 'alert',
            route: 'listGemba',
            key: i + responseJson.length + 1,
          });
        }
      });
  };

  getUser = async () => {
    const Name = await AsyncStorage.getItem('name');
    const Tipe = await AsyncStorage.getItem('tipe');
    this.setState({
      name: Name,
      tipe: Tipe,
    });
  };

  LogOut = async () => {
    await AsyncStorage.clear();
    this.props.navigation.navigate('authCheck');
  };

  Isi = content => content.content.map(item => (
    <ListItem
      button
      noBorder
      onPress={() => this.props.navigation.navigate(item.route, { type: item.name })
          }
      key={item.key}
    >
      <Left>
        <Icon
          active
          name={item.icon}
          style={{ color: '#777', fontSize: 26, width: 30 }}
        />
        <Text>{item.name}</Text>
      </Left>
    </ListItem>
  ));

  render() {
    return (
      <Container>
        <View style={{ backgroundColor: '#fdcb32' }}>
          <Image source={drawerCover} style={styles.sidebarImage} />
          <Text style={styles.sidebarName}>{this.state.name}</Text>
          <Text style={styles.sidebarTipe}>{this.state.tipe}</Text>
        </View>

        <Content style={{ flex: 1, backgroundColor: '#fff', top: -1 }}>
          <List
            dataArray={datas}
            renderRow={data => (
              <ListItem
                button
                noBorder
                onPress={() => this.props.navigation.navigate(data.route)}
              >
                <Left>
                  <Icon
                    active
                    name={data.icon}
                    style={{ color: '#777', fontSize: 26, width: 30 }}
                  />
                  <Text>{data.name}</Text>
                </Left>
              </ListItem>
            )}
          />
          <Accordion
            dataArray={menu}
            renderContent={this.Isi}
            headerStyle={{ backgroundColor: 'white', margin: 10 }}
            style={{ borderColor: 'white' }}
          />
        </Content>

        <TouchableOpacity
          style={{ marginBottom: 10, marginRight: 30 }}
          onPress={this.LogOut}
        >
          <Text style={styles.sidebarLogout}>Log Out</Text>
        </TouchableOpacity>
      </Container>
    );
  }
}

export default SideBar;
