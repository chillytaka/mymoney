import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  fab: {
    position: 'absolute', bottom: '90%', backgroundColor: '#5067FF',
  },
});
