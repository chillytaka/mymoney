import React from 'react';
import {
  Container, Content, Text, Fab, Icon,
} from 'native-base';
import { FlatList } from 'react-native';
import numeral from 'numeral';
import Header from '../common/header';
import Footer from '../common/footer';
import styles from './style';

export default class Report extends React.Component {
  render() {
    return (
      <Container>
        <Header title="Report" />
        <Content padder />
        <Fab style={styles.fab}>
          <Icon name="add" />
        </Fab>
        <Footer report />
      </Container>
    );
  }
}
