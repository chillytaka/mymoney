import { StyleSheet } from 'react-native';
import RF from 'react-native-responsive-fontsize';

export default StyleSheet.create({
  saldoText: {
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: RF(3),
    marginBottom: 10,
  },

  quickButton: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
});
