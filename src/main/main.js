import React from 'react';
import {
  Container, Content, Text,
} from 'native-base';
import { View } from 'react-native';
import numeral from 'numeral';
import Input from './textInput';
import Header from '../common/header';
import Footer from '../common/footer';
import QuickButton from './quickButton';
import styles from './style';

export default class Main extends React.Component {
  render() {
    return (
      <Container>
        <Header title="Main" />
        <Content padder>
          <Text style={styles.saldoText}>
            Saldo : Rp.
            {numeral(100000).format(0, 0)}
          </Text>
          <Input inputText="Setoran" />
          <QuickButton />
          <View style={{ marginVertical: '10%' }} />
          <Input inputText="Deposito" />
          <QuickButton />
        </Content>
        <Footer main />
      </Container>
    );
  }
}
