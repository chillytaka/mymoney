import React from 'react';
import PropTypes from 'prop-types';
import {
  Item, Button, Input, Text,
} from 'native-base';


export default class textInput extends React.Component {
    static propTypes = {
      inputText: PropTypes.string.isRequired,
    }

    render() {
      const { inputText } = this.props;
      return (
        <Item style={{ marginVertical: 5 }}>
          <Input placeholder={inputText} keyboardType="numeric" />
          <Button>
            <Text>Done</Text>
          </Button>
        </Item>
      );
    }
}
