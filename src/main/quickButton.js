import React from 'react';
import { View } from 'react-native';
import { Button, Text } from 'native-base';
import styles from './style';

export default class Quick extends React.Component {
  render() {
    return (
      <View style={{ marginVertical: 5 }}>
        <View style={[styles.quickButton, { marginBottom: 5 }]}>
          <Button rounded><Text>50.000</Text></Button>
          <Button rounded><Text>100.000</Text></Button>
          <Button rounded><Text>200.000</Text></Button>
        </View>
        <View style={styles.quickButton}>
          <Button rounded><Text>250.000</Text></Button>
          <Button rounded><Text>500.000</Text></Button>
          <Button rounded><Text>750.000</Text></Button>
        </View>
      </View>
    );
  }
}
