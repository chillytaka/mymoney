import React from 'react';
import {
  Form,
  Input,
  Item,
  Label,
  Container,
  Content,
  Text,
  Button,
  Toast,
} from 'native-base';
import { AsyncStorage, Image } from 'react-native';
import Error from '../common/errorToast';
import Loading from '../common/loading';

const img = require('../../assets/icon.png');


export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      pass: '',
      visible: false,
    };
  }

  LoggingIn = async () => {
    this.setState({ visible: true });
    fetch('https://kemarilah.net/Dehael/log.php', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        name: this.state.name,
        pass: this.state.pass,
      }),
    })
      .then(response => response.json()) // get response

      // act accroding to response
      .then((responseJson) => {
        if (responseJson.Message === 'failed') {
          this.setState({ visible: false });
          Toast.show({
            text: 'Nama atau Password salah',
            textStyle: { textAlign: 'center' },
            duration: 2000,
          });
        } else if (
          responseJson[0].TIPE === 'Register'
          || responseJson[0].TIPE === 'SUPER'
          || responseJson[0].TIPE === 'ADMIN'
        ) {
          AsyncStorage.setItem('name', responseJson[0].NAME);
          AsyncStorage.setItem('tipe', responseJson[0].TIPE);
          AsyncStorage.setItem('id', responseJson[0].ID);
          this.props.navigation.navigate('authCheck');
        } else {
          this.setState({ visible: false });
          Toast.show({
            text: 'Nama atau Password salah',
            textStyle: { textAlign: 'center' },
            duration: 2000,
          });
        }
      })
      .catch((error) => { Error(error); });
  };

  nextInput = (field) => {
    this[field]._root.focus();
  };

  render() {
    return (
      <Container style={{ backgroundColor: '#fdcb32' }}>
        <Content padder>
          <Image
            source={img}
            style={{
              alignSelf: 'center',
              resizeMode: 'contain',
              flex: 1,
              width: '80%',
            }}
          />
          <Form style={{ marginTop: '20%' }}>
            <Item floatingLabel>
              <Label style={{ padding: 5 }}>Name</Label>
              <Input
                onChangeText={(text) => {
                  this.setState({ name: text });
                }}
                onSubmitEditing={() => {
                  this.nextInput('pass');
                }}
                returnKeyType="next"
              />
            </Item>
            <Item floatingLabel>
              <Label style={{ padding: 5 }}>Password</Label>
              <Input
                getRef={(c) => {
                  this.pass = c;
                }}
                secureTextEntry
                onChangeText={(text) => {
                  this.setState({ pass: text });
                }}
              />
            </Item>
          </Form>
          <Button
            style={{ alignSelf: 'center', marginTop: 40 }}
            onPress={() => {
              this.LoggingIn();
            }}
          >
            <Text style={{ textAlign: 'center' }}>Login</Text>
          </Button>
        </Content>
        <Loading isLoading={this.state.visible} />
      </Container>
    );
  }
}
