import React from 'react';
import PropTypes from 'prop-types';
import {
  Header, Left, Right, Body, Title, Subtitle,
} from 'native-base';

export default class Atas extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    subtitle: PropTypes.string,
  }

  static defaultProps = {
    title: null,
    subtitle: null,
  }

  render() {
    return (
      <Header>
        <Left />
        <Body>
          <Title>{this.props.title}</Title>
          <Subtitle>{this.props.subtitle}</Subtitle>
        </Body>
        <Right />
      </Header>
    );
  }
}
