import React from 'react';
import PropTypes from 'prop-types';
import {
  Footer, Button, FooterTab, Text, Icon,
} from 'native-base';

export default class Bawah extends React.Component {
  static propTypes = {
    // change as necessary
    main: PropTypes.bool,
    report: PropTypes.bool,
    about: PropTypes.bool,
  }

  static defaultProps = {
    main: false,
    report: false,
    about: false,
  }

  constructor(props) {
    super(props);
    this.state = {
      draw: [],
    };
  }


  componentDidMount() {
    const { main, report, about } = this.props;
    this.setState({
      // place the menu list here
      draw: [
        {
          target: 'Main',
          icon: 'add',
          key: '1',
          text: 'Main',
          active: main,
        },
        {
          target: 'report',
          icon: 'analytics',
          text: 'Report',
          key: '2',
          active: report,
        },
        {
          target: 'About',
          icon: 'help-circle',
          text: 'About',
          key: '3',
          active: about,
        },
      ],
    });
  }

  render() {
    return (
      <Footer>
        <FooterTab>
          {this.state.draw.map(item => (
            <Button
              key={item.key}
              vertical
              active={item.active}
              onPress={() => { this.props.navigation.navigate(item.target); }}
            >
              <Icon name={item.icon} />
              <Text>{item.text}</Text>
            </Button>
          ))}
        </FooterTab>
      </Footer>
    );
  }
}
