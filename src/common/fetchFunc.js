import Error from './errorToast';

// change server destination as necessary
const server = 'https://kemarilah.net/Dehael/json';

// change PHP post header as necessary
const head = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
};

// get List of Report
export function getReport(id, tipe) {
  return new Promise((res) => {
    fetch(`${server}/data.php`, {
      method: 'POST',
      headers: head,
      body: JSON.stringify({
        tipe, id,
      }),
    })
      .then(response => response.json())
      .then((responseJson) => { res(responseJson); })
      .catch(error => Error(error));
  });
}

// get Details of Report
export function getReportDetails(id) {
  return new Promise((res) => {
    fetch(`${server}/details.php`, {
      method: 'POST',
      headers: head,
      body: JSON.stringify({
        id,
      }),
    })
      .then(response => response.json())
      .then((responseJson) => { res(responseJson); })
      .catch((error) => { Error(error); });
  });
}
