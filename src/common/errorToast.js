import { Toast } from 'native-base';

const Error = ({ errorText }) => {
  // comment on release
  // eslint-disable-next-line no-console
  console.log(errorText);
  Toast.show({
    text: errorText,
    buttonText: 'Okay',
    type: 'warning',
  });
};

export default Error;
