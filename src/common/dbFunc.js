import { SQLite } from 'expo';

// insert name of the db
const db = SQLite.openDatabase('audit.db');

// Get Gemba Question
export function getGemba(page = 0, type) {
  return new Promise((res) => {
    db.transaction((tx) => {
      tx.executeSql('select * from audit where AUDITTYPE=? limit ?,10',
        [type, page * 10],
        (_, { rows }) => { res(rows._array); });
    });
  });
}

// Storing Gemba Answer on Database
export function inputGemba(auditId, details, answer, img, site) {
  db.transaction((tx) => {
    tx.executeSql('insert or replace into audit_answer (AUDITID,ANSWER,IMG,SITE,DETAILS) values (?,?,?,?,?)',
      [auditId, answer, img, site, details]);
  });
}

export function getAnswer(auditId, site) {
  return new Promise((res) => {
    db.transaction((tx) => {
      tx.executeSql('select * from audit_answer where AUDITID=? and SITE=?',
        [auditId, site],
        (_, { rows }) => { res(rows._array); });
    });
  });
}
