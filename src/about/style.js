import { StyleSheet } from 'react-native';
import RF from 'react-native-responsive-fontsize';

export default StyleSheet.create({
  titleText: {
    textAlign: 'center',
    fontSize: RF(3),
  },
  detailsText: {
    textAlign: 'center',
    fontSize: RF(2.4),
    color: 'grey',
    marginVertical: 5,
  },
  footerText: {
    marginTop: '40%',
    color: 'lightgrey',
    fontSize: RF(2.4),
    textAlign: 'center',
  },
});
