/* eslint-disable global-require */
import React from 'react';
import { Container, Content, Text } from 'native-base';
import { Image } from 'react-native';
import Footer from '../common/footer';
import styles from './style';

export default class About extends React.Component {
  render() {
    return (
      <Container>
        <Content>
          <Image source={require('../../assets/icon.png')} style={{ alignSelf: 'center', marginVertical: '10%' }} />
          <Text style={styles.titleText}>Created by B14nk !</Text>
          <Text style={styles.detailsText}>Use with Caution</Text>
          <Text style={styles.detailsText}>Repository : </Text>
          <Text style={styles.detailsText}>https://gitlab.com/Blankon349/mymoney</Text>
          <Text style={styles.footerText}>created @ 2019, all rights reserved</Text>
        </Content>
        <Footer about />
      </Container>
    );
  }
}
